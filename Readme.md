#Illustrating Race Conditions in Java

## This App is supposed to illustrate race conditions in Java. It is based on an example from this blog: 
https://happycoding.io/tutorials/java-server/thread-safety

## Get Started
1. Start Server

## Single Thread
1. Open a Tabs in your Browser and go to "localhost:8080/doGet"
2. Click on the "+10" button and see what happens after 10sec.
3. After each 10sec you can repeat step 3. See what happens.

## Multi Thread
1. Open a second Tabs in your Browser and go to "localhost:8080/doGet"
2. Now that you have two tabs open click on "+10" in each tab quickly.
3. While the threads are running on the server click "Show Total" in any tab.

