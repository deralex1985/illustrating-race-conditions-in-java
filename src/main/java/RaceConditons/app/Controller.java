package RaceConditons.app;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@org.springframework.stereotype.Controller
public class Controller {

    private int total = 0;
    @GetMapping(path = "/doGet")
    public String doGet(Model model){
        model.addAttribute("total", total);
        System.out.println("Total from GET: "+ total);
    return "index.html";
    }

    @PostMapping(path = "/doPost")
    public String doPost(Model model){
        System.out.println("I was clicked");
        for(int i = 0; i < 10; i++){
            total++;
            System.out.println("Total from POST: "+ total);
            // this just delays the code by 1 second each time, so the request takes 10 seconds
            try {
                Thread.sleep(1000);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        model.addAttribute("total", total);
        return "index.html";
    }
}
